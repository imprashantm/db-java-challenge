# DB Java Challenge(money trasfer feature)

API's to allow to create, fetch account details and transfer money between accounts.

## Useful gradle Commands:
1. #####To start the server.


    ./gradlew startServer
    
NOTE: This will build and start the service on port 18080. The server will keep running in the background even after you exit the task.

1. #####To kill the instance of the running server.


    ./gradlew stopServer


#####Generating jacoco coverage report
jacoco test coverage report is generated at location `build/reports/jacoco/index.html` using the following command

    ./gradlew coverage

##Things pending:
1. generate resource documentation
1. add jmh for performance benchmarking
1. Create ErrorResponseBuilder for Validation and Service exception.

##Productionizing:
1. ####Performance:
    1. #####Java Microbenchmarking:
        Every service/unit should be covered using the JMH performance benchmarking to uncover any performance issues at micro level.
    1. #####Performance test simulations:
        Need to add user journey performance test simulations (Gatling test scripts) to ensure consistent response time for various user journeys. 
        Ideally the Response Time for each resource should be less than 500ms.
1. ####Scalability:
    1. #####Dynamic Scaling: 
        The application should be able to scale as and when needed dynamically, to ensure that the system can handle the expected load and provide consistent user experience.
    1. #####Load balancing: 
        Need to setup external urls/uris and Load Balancing(ELB/nginx) capabilities so that the underlying instances can be scaled dynamically to handle the load.
    1. #####ACID properties:
        1. Need transactional db, as the application requires to maintain ACID properties 
        1. The in-memory database is not a good candidate to achieve Scalability and maintain the ACID properties.    
1. ####Resilience:
    1. #####Chaos testing
        Need to load test the app to determine the breaking point(Gatling + Flood.io).
    1. #####Circuit breaking
        Need circuit breaking(Hystrix) from external systems such as the Notification Service to avoid bottlenecks due to external systems
1. ####Availability:
    1. #####Alerts and Monitoring:
        1. Need to add health check monitoring and alerts
        1. Need frameworks/tools for exposing the system logs(ELK stack)
1. ####CI-CD:
    1. #####Version control
        Setup version control and source code management for teams (Git)
    1. #####Environment setup:
        Need multiple environment setup to ensure dev, qa and performance readiness(AWS)
    1. #####Deployments:
        Need deployment pipelines and pipeline steps to ensure seamless and automated deployments across multiple environments(Ansible + Jenkins)
1. ####Portability and Environment Standardization:
    1. Need to make sure the code development and production usage environment are standardized.
    1. The service should be light weight and packaged to ensure portability and consistent behavior across environments(Docker) 
1. ####Documentation:
    1. Need api documentation for efficient and quick adoption of services/endpoints.