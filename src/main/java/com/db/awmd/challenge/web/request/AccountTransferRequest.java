package com.db.awmd.challenge.web.request;

import lombok.Getter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
public class AccountTransferRequest {
    @NotNull
    @NotEmpty
    String toAccountId;

    @NotNull
    @Min(value = 0, message = "Transfer amount must be positive.")
    BigDecimal amount;
}
