package com.db.awmd.challenge.repository;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.exception.DuplicateAccountIdException;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.db.awmd.challenge.exception.InsufficentFundsException;
import com.db.awmd.challenge.exception.InvalidAmountException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import static java.lang.String.format;

@Repository
@Slf4j
public class AccountsRepositoryInMemory implements AccountsRepository {

  private final Map<String, Account> accounts = new ConcurrentHashMap<>();

  @Override
  public void createAccount(Account account) throws DuplicateAccountIdException {
    Account previousAccount = accounts.putIfAbsent(account.getAccountId(), account);
    if (previousAccount != null) {
      throw new DuplicateAccountIdException(
        "Account id " + account.getAccountId() + " already exists!");
    }
  }

  @Override
  public Account getAccount(String accountId) {
    return accounts.get(accountId);
  }

  @Override
  public void clearAccounts() {
    accounts.clear();
  }

  @Override
  public Account updateAccount(Account account) {
    synchronized (accounts) {
      return accounts.put(account.getAccountId(), account);
    }
  }

  @Override
  public Account debit(String fromAccountId, BigDecimal amount) {
    synchronized (accounts){
      if(amount.compareTo(BigDecimal.valueOf(0))<0){
        throw new InvalidAmountException("Negative amount cannot be debited from account");
      }
      Account fromAccount = getAccount(fromAccountId);
      if(fromAccount.getBalance().compareTo(amount)<0){
        throw new InsufficentFundsException(format("Account with id %s has insufficient funds to transfer amount of %s", fromAccountId, amount.toString()));
      }
      fromAccount.setBalance(fromAccount.getBalance().subtract(amount));
      log.info("amount {} debited from account {}, account balance is {}",amount, fromAccountId, fromAccount.getBalance());
      return updateAccount(fromAccount);
    }
  }

  @Override
  public Account credit(String toAccountId, BigDecimal amount) {
    synchronized (accounts){
      if(amount.compareTo(BigDecimal.valueOf(0))<0){
        throw new InvalidAmountException("Negative amount cannot be credited to account");
      }
      Account toAccount = getAccount(toAccountId);
      toAccount.setBalance(toAccount.getBalance().add(amount));
      log.info("amount {} credited to account {}, account balance is {}",amount, toAccountId, toAccount.getBalance());
      return updateAccount(toAccount);
    }
  }

}
