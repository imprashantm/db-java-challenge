package com.db.awmd.challenge.service;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.exception.AccountNotFoundException;
import com.db.awmd.challenge.repository.AccountsRepository;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

import static java.lang.String.format;

@Service
@Slf4j
public class AccountsService {

    @Getter
    private final AccountsRepository accountsRepository;

    @Getter
    private final NotificationService notificationService;

    public final static String DEBIT_MSG = "Amount %s debited from account %s";
    public final static String CREDIT_MSG = "Amount %s credited from account %s";

    @Autowired
    public AccountsService(AccountsRepository accountsRepository, NotificationService notificationService) {
        this.accountsRepository = accountsRepository;
        this.notificationService = notificationService;
    }

    public void createAccount(Account account) {
        this.accountsRepository.createAccount(account);
    }

    public Account getAccount(String accountId) {
        return Optional.ofNullable(accountsRepository.getAccount(accountId)).orElseThrow(() -> new AccountNotFoundException(accountId, format("Account with id %s does not exist", accountId)));
    }

    public boolean transferMoney(String fromAccountId, String toAccountId, BigDecimal amountToTransfer) {
        Account fromAccount = getAccount(fromAccountId);
        Account toAccount = getAccount(toAccountId);

        debit(fromAccount, amountToTransfer);
        credit(toAccount, amountToTransfer);

        return true;
    }

    private void debit(Account account, BigDecimal amountToTransfer){
        accountsRepository.debit(account.getAccountId(), amountToTransfer);
        sendDebitNotification(account, amountToTransfer);
    }
    private void credit(Account account, BigDecimal amountToTransfer){
        accountsRepository.credit(account.getAccountId(), amountToTransfer);
        sendCreditNotification(account, amountToTransfer);
    }

    private void sendDebitNotification(Account account, BigDecimal amountToTransfer){
        try{
            notificationService.notifyAboutTransfer(account, format(DEBIT_MSG, amountToTransfer, account.getAccountId()));
        }catch (RuntimeException e){
            log.error("Error sending debit notification to {} of amount {} : {}", account.getAccountId(), amountToTransfer, e);
        }
    }

    private void sendCreditNotification(Account account, BigDecimal amountToTransfer){
        try{
            notificationService.notifyAboutTransfer(account, format(CREDIT_MSG, amountToTransfer, account.getAccountId()));
        }catch (RuntimeException e){
            log.error("Error sending credit notification to {} of amount {} : {}", account.getAccountId(), amountToTransfer, e);
        }
    }
}
