package com.db.awmd.challenge.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Insufficient funds")
public class InsufficentFundsException extends RuntimeException{
    public InsufficentFundsException(String message) {
        super(message);
    }
}
