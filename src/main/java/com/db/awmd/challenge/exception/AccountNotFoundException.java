package com.db.awmd.challenge.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Account not found")
public class AccountNotFoundException extends RuntimeException {
    private String accountId;
    public AccountNotFoundException(String accountId, String message) {
        super(message);
        this.accountId = accountId;
    }
}
