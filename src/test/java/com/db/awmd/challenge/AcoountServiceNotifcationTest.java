package com.db.awmd.challenge;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.repository.AccountsRepository;
import com.db.awmd.challenge.service.AccountsService;
import com.db.awmd.challenge.service.EmailNotificationService;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AcoountServiceNotifcationTest {

    @Mock
    private
    AccountsRepository accountsRepository;

    @Mock
    private
    EmailNotificationService emailNotificationService;

    @InjectMocks
    private
    AccountsService accountsService;

    private String FROM_ACCOUNT_ID = "from-account-123";
    private String TO_ACCOUNT_ID = "to-account-123";
    private BigDecimal FROM_ACCOUNT_BALANCE = new BigDecimal("1000");
    private BigDecimal TO_ACCOUNT_BALANCE = new BigDecimal("500");
    private BigDecimal AMOUNT_TO_TRANSFER = new BigDecimal("500");
    private Account fromAccount;
    private Account toAccount;
    private String DEBIT_MSG;
    private String CREDIT_MSG;

    @Before
    public void setUp(){
        fromAccount = new Account(FROM_ACCOUNT_ID, FROM_ACCOUNT_BALANCE);
        toAccount = new Account(TO_ACCOUNT_ID, TO_ACCOUNT_BALANCE);
        DEBIT_MSG = String.format(AccountsService.DEBIT_MSG, AMOUNT_TO_TRANSFER.toString(), FROM_ACCOUNT_ID);
        CREDIT_MSG = String.format(AccountsService.CREDIT_MSG, AMOUNT_TO_TRANSFER.toString(), TO_ACCOUNT_ID);
    }

    @Test
    public void transferMoney_shouldSendEmailNotification() {
        //given
        Mockito.when(accountsRepository.getAccount(FROM_ACCOUNT_ID)).thenReturn(fromAccount);
        Mockito.when(accountsRepository.getAccount(TO_ACCOUNT_ID)).thenReturn(toAccount);

        //when
        boolean success = accountsService.transferMoney(FROM_ACCOUNT_ID, TO_ACCOUNT_ID, AMOUNT_TO_TRANSFER);

        //then
        verify(emailNotificationService, times(1)).notifyAboutTransfer(fromAccount, DEBIT_MSG);
        verify(emailNotificationService, times(1)).notifyAboutTransfer(toAccount, CREDIT_MSG);
        Assertions.assertThat(success).isTrue();
    }

    @Test
    public void transferMoney_shouldTransferMoney_creditNotificationFails() {
        //given
        Mockito.when(accountsRepository.getAccount(FROM_ACCOUNT_ID)).thenReturn(fromAccount);
        Mockito.when(accountsRepository.getAccount(TO_ACCOUNT_ID)).thenReturn(toAccount);
        doThrow(new RuntimeException()).when(emailNotificationService).notifyAboutTransfer(toAccount, CREDIT_MSG);

        //when
        boolean success = accountsService.transferMoney(FROM_ACCOUNT_ID, TO_ACCOUNT_ID, AMOUNT_TO_TRANSFER);

        //then
        verify(emailNotificationService, times(1)).notifyAboutTransfer(fromAccount, DEBIT_MSG);
        verify(emailNotificationService, times(1)).notifyAboutTransfer(toAccount, CREDIT_MSG);
        Assertions.assertThat(success).isTrue();
    }

    @Test
    public void transferMoney_shouldTransferMoney_debitNotificationFails() {
        //given
        Mockito.when(accountsRepository.getAccount(FROM_ACCOUNT_ID)).thenReturn(fromAccount);
        Mockito.when(accountsRepository.getAccount(TO_ACCOUNT_ID)).thenReturn(toAccount);
        doThrow(new RuntimeException()).when(emailNotificationService).notifyAboutTransfer(fromAccount, DEBIT_MSG);

        //when
        boolean success = accountsService.transferMoney(FROM_ACCOUNT_ID, TO_ACCOUNT_ID, AMOUNT_TO_TRANSFER);

        //then
        verify(emailNotificationService, times(1)).notifyAboutTransfer(fromAccount, DEBIT_MSG);
        verify(emailNotificationService, times(1)).notifyAboutTransfer(toAccount, CREDIT_MSG);
        Assertions.assertThat(success).isTrue();
    }
}
