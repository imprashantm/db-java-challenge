package com.db.awmd.challenge;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.exception.AccountNotFoundException;
import com.db.awmd.challenge.exception.DuplicateAccountIdException;
import com.db.awmd.challenge.exception.InsufficentFundsException;
import com.db.awmd.challenge.service.AccountsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountsServiceTest {

    @Autowired
    private AccountsService accountsService;
    @Before
    public void setUp(){
        // Reset the existing accounts before each test.
        accountsService.getAccountsRepository().clearAccounts();
    }

    @Test
    public void addAccount() throws Exception {
        Account account = new Account("Id-123", new BigDecimal(1000));
        accountsService.createAccount(account);

        assertThat(accountsService.getAccount("Id-123")).isEqualTo(account);
    }

    @Test
    public void addAccount_failsOnDuplicateId() throws Exception {
        String uniqueId = "Id-" + System.currentTimeMillis();
        Account account = new Account(uniqueId);
        accountsService.createAccount(account);

        try {
            accountsService.createAccount(account);
            fail("Should have failed when adding duplicate account");
        } catch (DuplicateAccountIdException ex) {
            assertThat(ex.getMessage()).isEqualTo("Account id " + uniqueId + " already exists!");
        }
    }

    @Test
    public void transferMoney_shouldTransferMoney(){
        //given
        String fromAccountId = "Id-123";
        Account fromAccount = new Account(fromAccountId, new BigDecimal("1000"));
        accountsService.createAccount(fromAccount);
        String toAccountId = "Id-456";
        Account toAccount = new Account(toAccountId, new BigDecimal("1000"));
        accountsService.createAccount(toAccount);
        BigDecimal amountToTransfer = BigDecimal.valueOf(500);

        //when
        accountsService.transferMoney(fromAccountId, toAccountId, amountToTransfer);

        //then
        fromAccount = accountsService.getAccount(fromAccountId);
        toAccount = accountsService.getAccount(toAccountId);
        assertThat(fromAccount.getBalance()).isEqualByComparingTo("500");
        assertThat(toAccount.getBalance()).isEqualByComparingTo("1500");
    }

    @Test(expected = AccountNotFoundException.class)
    public void invalidFromAccountId_transferMoney_shouldThrowAccountNotFoundException(){
        //given
        String fromAccountId = "InvalidId";
        String toAccountId = "Id-456";
        Account toAccount = new Account(toAccountId, new BigDecimal("1000"));
        accountsService.createAccount(toAccount);
        BigDecimal amountToTransfer = BigDecimal.valueOf(500);

        //when
        accountsService.transferMoney(fromAccountId, toAccountId, amountToTransfer);

        //then
        //AccountNotFoundException is thrown
    }

    @Test(expected = AccountNotFoundException.class)
    public void invalidToAccountId_transferMoney_shouldThrowAccountNotFoundException(){
        //given
        String fromAccountId = "Id-123";
        Account fromAccount = new Account(fromAccountId, new BigDecimal("1000"));
        accountsService.createAccount(fromAccount);
        String toAccountId = "InvalidId";
        BigDecimal amountToTransfer = BigDecimal.valueOf(500);

        //when
        accountsService.transferMoney(fromAccountId, toAccountId, amountToTransfer);

        //then
        //AccountNotFoundException is thrown
    }

    @Test(expected = InsufficentFundsException.class)
    public void insufficientFundsInFromAccount_transferMoney_shouldThrowInsufficientFundsException(){
        //given
        String fromAccountId = "Id-123";
        Account fromAccount = new Account(fromAccountId, new BigDecimal("100"));
        accountsService.createAccount(fromAccount);
        String toAccountId = "Id-456";
        Account toAccount = new Account(toAccountId, new BigDecimal("1000"));
        accountsService.createAccount(toAccount);
        BigDecimal amountToTransfer = new BigDecimal("500");

        //when
        accountsService.transferMoney(fromAccountId, toAccountId, amountToTransfer);
    }

    @Test
    public void multithreadedTransferMoney_shouldTransferMoney() throws InterruptedException {
        //given
        String fromAccountId = "Id-123";
        Account fromAccount = new Account(fromAccountId, new BigDecimal("1000"));
        accountsService.createAccount(fromAccount);
        String toAccountId = "Id-456";
        Account toAccount = new Account(toAccountId, new BigDecimal("1000"));
        accountsService.createAccount(toAccount);
        BigDecimal amountToTransfer = BigDecimal.valueOf(250);

        //when
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        List<Callable<Boolean>> transferMoneyRequests = Arrays.asList(
                () -> accountsService.transferMoney(fromAccountId, toAccountId, amountToTransfer),
                () -> accountsService.transferMoney(fromAccountId, toAccountId, amountToTransfer),
                () -> accountsService.transferMoney(fromAccountId, toAccountId, amountToTransfer),
                () -> accountsService.transferMoney(fromAccountId, toAccountId, amountToTransfer));
        final List<Future<Boolean>> futures = executorService.invokeAll(transferMoneyRequests);


        //then
        fromAccount = accountsService.getAccount(fromAccountId);
        toAccount = accountsService.getAccount(toAccountId);
        assertThat(fromAccount.getBalance()).isEqualByComparingTo("0");
        assertThat(toAccount.getBalance()).isEqualByComparingTo("2000");
        futures.stream().map(future -> {
            try {
                return assertThat(future.get()).isEqualTo(true);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return false;
            }
        });
        executorService.shutdown();
    }

}
