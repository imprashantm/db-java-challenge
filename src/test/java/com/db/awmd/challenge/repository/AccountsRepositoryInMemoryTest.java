package com.db.awmd.challenge.repository;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.exception.InvalidAmountException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountsRepositoryInMemoryTest {

    @Autowired
    private
    AccountsRepository accountsRepository;

    @Before
    public void setUp(){
        // Reset the existing accounts before each test.
        accountsRepository.clearAccounts();
    }
    @Test
    public void debit_shouldDebitAmount() {
        //given
        Account account = new Account("Id-123", new BigDecimal(1000));
        accountsRepository.createAccount(account);

        //when
        accountsRepository.debit(account.getAccountId(), new BigDecimal(500));

        //then
        account = accountsRepository.getAccount(account.getAccountId());
        assertThat(account.getBalance()).isEqualByComparingTo("500");
    }

    @Test(expected = InvalidAmountException.class)
    public void debit_shouldNotDebitInvalidAmount() {
        //given
        Account account = new Account("Id-123", new BigDecimal(1000));
        accountsRepository.createAccount(account);

        //when
        accountsRepository.debit(account.getAccountId(), new BigDecimal(-500));

        //then
        account = accountsRepository.getAccount(account.getAccountId());
        assertThat(account.getBalance()).isEqualByComparingTo("1000");
    }

    @Test
    public void multithreadedDebit_shouldDebitAmount() throws InterruptedException {
        //given
        String accountId = "Id-123";
        Account account = new Account(accountId, new BigDecimal(2000));
        accountsRepository.createAccount(account);

        //when
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        List<Callable<Account>> transferMoneyRequests = Arrays.asList(
                () -> accountsRepository.debit(accountId, new BigDecimal(300)),
                () -> accountsRepository.debit(accountId, new BigDecimal(700)),
                () -> accountsRepository.debit(accountId, new BigDecimal(250)),
                () -> accountsRepository.debit(accountId, new BigDecimal(750)));
        final List<Future<Account>> futures = executorService.invokeAll(transferMoneyRequests);

        //then
        account = accountsRepository.getAccount(account.getAccountId());
        assertThat(account.getBalance()).isEqualByComparingTo("0");
    }

    @Test
    public void credit() {
        //given
        Account account = new Account("Id-123", new BigDecimal(0));
        accountsRepository.createAccount(account);

        //when
        accountsRepository.credit(account.getAccountId(), new BigDecimal(500));

        //then
        account = accountsRepository.getAccount(account.getAccountId());
        assertThat(account.getBalance()).isEqualByComparingTo("500");
    }

    @Test(expected = InvalidAmountException.class)
    public void credit_shouldFailOnInvalidAmount() {
        //given
        Account account = new Account("Id-123", new BigDecimal(1000));
        accountsRepository.createAccount(account);

        //when
        accountsRepository.credit(account.getAccountId(), new BigDecimal(-500));

        //then
        account = accountsRepository.getAccount(account.getAccountId());
        assertThat(account.getBalance()).isEqualByComparingTo("500");
    }

    @Test
    public void multithreadedCredit_shouldCreditRightAmount() throws InterruptedException {
        //given
        String accountId = "Id-123";
        Account account = new Account(accountId, new BigDecimal(0));
        accountsRepository.createAccount(account);

        //when
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        List<Callable<Account>> transferMoneyRequests = Arrays.asList(
                () -> accountsRepository.credit(accountId, new BigDecimal(300)),
                () -> accountsRepository.credit(accountId, new BigDecimal(700)),
                () -> accountsRepository.credit(accountId, new BigDecimal(250)),
                () -> accountsRepository.credit(accountId, new BigDecimal(750)));
        final List<Future<Account>> futures = executorService.invokeAll(transferMoneyRequests);

        //then
        account = accountsRepository.getAccount(account.getAccountId());
        assertThat(account.getBalance()).isEqualByComparingTo("2000");
    }
}